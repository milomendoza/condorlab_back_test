# Web Condorlab-test -- Backend


### Commands

**npm i**

**node server.js**

### Get
Returns all records from the providers collection

```GET.HTTP```

http://localhost:8090/api/providers


### Get
Returns an specific provider from the providers collection, given a providerId

params:
**providerId**

```GET.HTTP```

http://localhost:8090/api/providers/providerId
  

### Delete
Deletes a provider in the providers collection, given a providerId

params:
**providerId**

```DELETE.HTTP```

http://localhost:8090/api/providers/providerId


### Post
Creates a new provider and saves it in the providers collection

body:
** Type ** : Object


** json ** : { firstName, lastName, middleName, email, specialtyId, projectedStartDate, employerId, providerType, staffStatus, assignedTo, status, createdBy }

```POST.HTTP```

http://localhost:8090/api/providers


### Put
Udates a provider and saves it in the providers collection, given a providerId

params:
**providerId**

body:
** Type ** : Object

** json ** : { firstName, lastName, middleName, email, specialtyId, projectedStartDate, employerId, providerType, staffStatus, assignedTo, status, createdBy }

```PUT.HTTP```

http://localhost:8090/api/providers/providerId


